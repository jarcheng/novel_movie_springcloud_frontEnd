import axios from 'axios'
import router from './src/router'
import Vue from 'vue'
import {Toast} from 'vant';

// const base = "http://192.168.31.150:81/api"
const base = "/api"
// const base = "http://47.92.95.86:8080"

const http = axios.create({

        baseURL: base,
        headers: {"vary": "Access-Control-Request-Headers", "Content-Type": "application/json"}
    }
)
http.baseURL = base

http.interceptors.request.use(function (config) {
    if (localStorage.getItem('token')) {
        config.headers.Authorization = 'Bearer ' + localStorage.getItem('token')
    }
    return config;
}, function (error) {
    return Promise.reject(error);
});

http.interceptors.response.use(function (response) {
    if ((20000 <= response.data.code && response.data.code <= 30000) || response.data.code >= 70003) {
        Toast.fail(response.data.message)
        router.push("/login")
    }
    return response;
});

export default http