import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '@/views/Home.vue'
import register from "@/views/register";
import login from "@/views/login";
import userinfo from "@/views/userinfo";
import article from "@/views/article";
import searchResult from "@/views/searchResult";
import {Tab, Tabs} from 'vant';
import edit from "@/components/UserComponents/edit";
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import admin from "@/views/admin";
import userCollect from "@/components/adminCommon/userCollect";
import MovieManger from "@/components/adminCommon/MovieManger";
import novelManager from "@/components/adminCommon/NovelManager";
import novelShow from "@/views/novelShow";
import userHistory from "@/components/UserComponents/userHistory";
import userCollect2 from "@/components/UserComponents/userCollect"
import UserManager from "@/components/adminCommon/UserManager";
import PermissionManager from "@/components/adminCommon/PermissionManager";
import ConnectVip from "@/components/UserComponents/ConnectVip";
import Payment from "@/components/UserComponents/Payment";
import UserOrder from "@/components/adminCommon/UserOrder";
import UserVideoManager from "@/components/adminCommon/UserVideoManager";
import AdminLogin from "@/components/adminCommon/AdminLogin";
import faceLogin from "@/views/faceLogin";
import chat from "../views/chat";
import message from "../views/message";
import visitor from "../views/visitor";
import likes from "../views/likes";

Vue.use(ElementUI);
Vue.use(Tab);
Vue.use(Tabs);
Vue.use(VueRouter)
const originalPush = VueRouter.prototype.push
VueRouter.prototype.push = function push(location) {
    return originalPush.call(this, location).catch(err => err)
}
const routes = [
    {
        path: "/admin",
        name: "admin",
        component: admin,
        redirect: '/userManage',
        children: [
            {
                path: '/collect',
                component: userCollect
            },
            {
                path: '/MovieManger',
                component: MovieManger
            },
            {
                path: '/novelManager',
                component: novelManager
            },
            {
                path: '/userManage',
                component: UserManager
            },
            {
                path: "/permissionManager",
                component: PermissionManager
            },
            {
                path: "/orders",
                component: UserOrder
            },
            {
                path: "/UserVideoManager",
                component: UserVideoManager
            }
        ]

    },
    {
        path: "/faceLogin",
        component: faceLogin
    },
    {
        path: "/adminLogin",
        component: AdminLogin
    },
    {
        path: "/novel",
        name: "novel",
        component: novelShow
    },
    {
        path: "/connectVip",
        component: ConnectVip
    },
    {
        path: "/payment",
        component: Payment
    },
    {
        path: "/edit",
        name: "edit",
        component: edit
    },
    {
        path: "/searchResult/:id",
        name: "searchResult",
        component: searchResult,
        meta: {
            keepAlive: true
        }
    },
    {
        path: '/article',
        name: 'article',
        component: article
    },
    {
        path: '/history',
        name: 'userHistory',
        component: userHistory
    },
    {
        path: '/userCollect',
        name: 'userCollect',
        component: userCollect2
    },
    {
        path: '/',
        name: 'Home',
        component: Home,
        meta: {
            keepAlive: true
        }
    },
    {
        path: '/login',
        component: login
    },
    {
        path: '/userinfo/:time',
        component: userinfo
    },
    {
        path: '/about',
        name: 'About',
        // route level code-splitting
        // this generates a separate chunk (about.[hash].js) for this route
        // which is lazy-loaded when the route is visited.
        component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
    },
    {
        path: '/register',
        name: 'Register',
        component: register
    },
    {
        path: '/message',
        name: 'message',
        component: message
    },
    {
        path: '/chat',
        name: 'chat',
        component: chat
    },
    {
        path: '/visitor',
        name: 'visitor',
        component: visitor
    },
    {
        path: '/likes',
        name: "likes",
        component: likes
    }
]

const router = new VueRouter({
    mode: 'hash',
    base: process.env.BASE_URL,
    routes
})

export default router
